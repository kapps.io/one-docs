# Getting Started

In this documentation we will cover only **One CMS** related stuff. If you want you learn more about processwire, please check the [official docs](http://processwire.com/docs/).

::: warning Requirements
PHP 7, PHP's bundled GD 2 library, MySQL 5.5+, Apache must have mod_rewrite enabled, Apache must support .htaccess files
:::

## Installation
