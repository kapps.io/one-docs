---
title: 'One Docs'
---

<img src="./one.png" alt="One" height="80">

# One Page Template System

**One CMS** is one page template system built on top of **processwire** CMF, and it's powered by **UIkit** front-end framework, on the back and front-end.    

::: tip Sections Builder
Page builder and uniq section design system, for building and managing your homepage.
:::

It's designed for building single or multi-page websites, with scroll and standard page navigation.  
Main design features and page-builder is focused on a homepage, but **stand-alone pages** are fully supported, same as structured content via modules and custom UI. It has powerful **layout manager** system that lets you build and switch between different page and section layouts with ease.

::: tip Stand Alone Pages
Full support for stand alone pages and page templates. Build advanced page layouts and manage them in built-in layout-manager.
:::

One CMS has custom UI for managing pages, and next to standard pages, it also supports modals and off-canvas that you can invoke from anywhere on website.

::: tip Modules & Addons
Default processwire modules support and custom addon system for customizations and new functionality.
:::

**One CMS** is very easy to use, not just for managing content, but also customizing your website. Change fonts, colors, and various elements layouts/design directly from admin interface, no coding required.

::: tip Customizer & Layout Manager
Manage your fonts, colors, spaces, page and section layouts with built-in customizer & layout-manager.
:::

<h2>Cons & Pros</h2>

::: tip Pros
* Super easy to use
* Multilanguage support out of the box
* Very flexible
* Homepage section builder
* 20+ Sections out of the box
* Customizer (manage your fonts, colors, spaces...)
* Layout manager system. Switch between sections and pages layouts on fly.
* Built-in Form Builder
* Shortcodes
* Stand alone pages, structured content, modals, off-canvas.
* Fully responsive
* Highly extendable
* ...
:::

::: warning Cons
* No support for navbar dropdown menus.
* Structured content (eg: blog->blog-post) supported only via content modules. This is not necessarily a con, structured content is supported via modules and custom UI (like built-in News), wich makes it super easy to use for end-users, but it's harder to build.
* No easy way to switch between different themes on fly, like in other popular systems. Layout manager solves this by allowing you to build and switch between different elements layouts across the site.
:::
