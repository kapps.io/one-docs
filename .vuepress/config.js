module.exports = {
    title: 'One CMS',
    description: 'One CMS documenatation and tutorials',
    base: "/",
	head: [
		['link', { rel: 'icon', href: '/one.png' }]
	],

    themeConfig: {

        nav: [
            { text: 'Intro', link: '/' },
            { text: 'Basics', link: '/basics/' },
            { text: 'Advanced', link: '/advanced/' },
            { text: 'kapps.io', link: 'http://kapps.io' },
        ],

        sidebar: {
            '/basics/': [
                '',
            ],

            '/advanced/': [
                '',
            ],

            // main page fallback, must be last
            '/': [
                ['/', 'Intro'],
                ['/basics/', 'Basics'],
                ['/advanced/', 'Advanced']
            ],
        }

    }
}
